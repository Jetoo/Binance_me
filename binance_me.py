import datetime
import requests
import json
from binance.spot import Spot as Client
from time import sleep
from telegram import Bot

sendToTelegram = 1  # если 1, то отправлять сообщения в телеграм (если 0, то просто выводить на экран)
telegramBotToken = '1911395864:AAFXG0zOojYgw6_mSwtuoDWHgx9EnAmOMKI' # ID телеграм-бота, который отправляет сообщения
telegramChatId = '-1001707956140'                                   # ID чата, куда отправляются сообщения
bot = Bot(telegramBotToken)

prevBestDeals = {}
rates = {
    'USDT_RUB_buy':  4,  # макс. выгодная наценка при покупке USDT за RUB с банковской карты (перевод по СБП, Тинькофф и др.)
    'USDT_RUB_sell': 6,  # мин. выгодная наценка при продаже USDT за RUB на банковскую карту (перевод по СБП, Тинькофф и др.)
    'USDT_USD_buy':  4,  # макс. выгодная наценка при покупке USDT за USD с банковской карты (только Тинькофф)
    'USDT_USD_sell': 8,  # мин. выгодная наценка при продаже USDT за USD на банковскую карту (только Тинькофф)
    'RUB_RUB_buy':   4,  # макс. выгодная наценка при покупке для фиатного RUB за RUB с банковской карты (перевод по СБП, Тинькофф и др.)
    'RUB_RUB_sell':  8,  # мин. выгодная наценка при продаже для фиатного RUB за RUB с банковской карты (перевод по СБП, Тинькофф и др.)
    # 'USDT_RUB_buy': 10,
    # 'USDT_RUB_sell': 1,
    # 'USDT_USD_buy': 10,
    # 'USDT_USD_sell': 1,
    # 'RUB_RUB_buy': 10,
    # 'RUB_RUB_sell': 1,
}

# отправить сообщение в чат
def sendNotification(text):
    if sendToTelegram:
        try:
            bot.get_me()
        except:
            bot = Bot(telegramBotToken)
        bot.sendMessage(telegramChatId, text)
    print(f'{text}\n')

# показать лучшую сделку
def showBestDeal(title,deal,interest,rate,isBuy=True,market=0):
    if (isBuy and interest < rates[rate] or not isBuy and interest >= rates[rate]) \
        and prevBestDeals.get(rate) != deal['advNo']: # если наценка выгодна и это новый ордер, то отправить сообщение
        marketText = f'\n    Рыночная цена: {market}' if market > 0 else ''
        sendNotification(
            f'''{title} по {deal['price']}{marketText}
    Наценка: {interest}%
    Доступно: {deal['avAmount']}
    Лимиты: от {deal['minAmount']} до {deal['maxAmount']}
    Продавец: {deal['nickName']} ({deal['orderCount']} сделок, {deal['orderRate']}% успешных)
    Способы: {deal['methods']}''')
        prevBestDeals[rate] = deal['advNo'] # запомнить ID ордера, чтобы не выводить его повторно

# преобразование наценки в читаемый вид
def makeRateNicer(number):
    return round(number*100,1)

# получить лучшую цену
def getBestDeal(asset, fiat, payTypes,tradeType='BUY'):
    data = {
        'asset': asset,
        'fiat': fiat,
        'merchantCheck': False, # показывать только мерчантов
        'page': 1,
        'payTypes': payTypes,
        'publisherType': None,
        'rows': 1,
        'tradeType': tradeType
        }

    r = requests.post('https://p2p.binance.com/bapi/c2c/v2/friendly/c2c/adv/search', json=data)
    str = json.loads(r.text)
    data = str['data'][0]
    adv = data['adv']
    advertiser = data['advertiser']
    tradeMethods = [i['tradeMethodName'] for i in adv['tradeMethods']]

    return {
        'advNo': float(adv['advNo']),                                            # ID ордера
        'methods': ', '.join(tradeMethods),                                      # методы перевода
        'price': float(adv['price']),                                            # цена
        'nickName': advertiser['nickName'],                                      # ник юзера ордера
        'orderCount': int(advertiser['monthOrderCount']),                        # ордеров за последние 30 дней
        'orderRate': makeRateNicer(advertiser['monthFinishRate']),               # % успешных ордеров за последние 30 дней
        'maxAmount': f"{adv['fiatSymbol']}{adv['dynamicMaxSingleTransAmount']}", # макс. лимит суммы
        'minAmount': f"{adv['fiatSymbol']}{adv['minSingleTransAmount']}",        # мин. лимит суммы
        'avAmount': f"{adv['tradableQuantity']} {adv['asset']}",                 # доступные средства юзера
        }

waitTime = 5    # время ожидания в секундах до запуска следующей проверки

tradeAmountUSD = 500    # пока не используется - сумма сделки в USD (для фильтрации слишком больших ордеров)
tradeAmountRUB = 10000  # пока не используется - сумма сделки в RUB (для фильтрации слишком больших ордеров)

sendNotification(
    f'''Бот перезапущен.
    USDT/RUB:
        макс. покупка {rates['USDT_RUB_buy']}%
        мин. продажа  {rates['USDT_RUB_sell']}%
    USDT/USD:
        макс. покупка {rates['USDT_USD_buy']}%
        мин. продажа  {rates['USDT_USD_sell']}%
    Фиат RUB/RUB:
        макс. покупка {rates['RUB_RUB_buy']}%
        мин. продажа  {rates['RUB_RUB_sell']}%''')

RUB_payTypes = [                # способы платежей для операций с RUB
    'SpecificBank',
    'Bank',
    'Tinkoff',
    'ABank',
    'HomeCreditBank'
    'RaiffeisenBankRussia',
    'BCSBank',
    'RussianStandardBank',
    'RosBank',
    'MTSBank',
    'FPS',
    'FasterPayments',
    # 'MobileTopUp',
    # 'TBCBank',
    # 'BaknOfGeorgia',
]

USD_payTypes = [                # способы платежей для операций с USD
    'Tinkoff'
]

spot_client = Client(base_url='https://api.binance.com')

while True:
    print(f'{datetime.datetime.now()}\n--------------------------')

    try:
        USDT_RUB_rate = float(spot_client.ticker_price('USDTRUB')['price']) # получение рыночной цены USDT/RUB

        # покупка USDT за рубли
        bestDeal = getBestDeal('USDT','RUB',RUB_payTypes)
        bestInterest = makeRateNicer(1-USDT_RUB_rate/bestDeal['price'])
        showBestDeal(f'🔴⬆️\nUSDT/RUB: покупай',bestDeal,bestInterest,'USDT_RUB_buy',market=USDT_RUB_rate)

        # продажа USDT за рубли
        bestDeal = getBestDeal('USDT','RUB',RUB_payTypes,'SELL')
        price = bestDeal['price']
        bestInterest = makeRateNicer(bestDeal['price']/USDT_RUB_rate-1)
        showBestDeal(f'🔴⬇️\nUSDT/RUB: продавай',bestDeal,bestInterest,'USDT_RUB_sell',False,USDT_RUB_rate)

        # покупка USDT за доллары
        bestDeal = getBestDeal('USDT','USD',USD_payTypes)
        bestInterest = makeRateNicer(bestDeal['price']-1)
        showBestDeal(f'🟠⬆️\nUSDT/USD: покупай',bestDeal,bestInterest,'USDT_USD_buy')

        # продажа USDT за доллары
        bestDeal = getBestDeal('USDT','USD',USD_payTypes,'SELL')
        bestInterest = makeRateNicer(bestDeal['price']-1)
        showBestDeal(f'🟠⬇️\nUSDT/USD: продавай',bestDeal,bestInterest,'USDT_USD_sell',False)

        # покупка фиатных рублей за рубли
        bestDeal = getBestDeal('RUB','RUB',RUB_payTypes)
        bestInterest = makeRateNicer(bestDeal['price']-1)
        showBestDeal(f'🔵⬆️\nФиат RUB/RUB: покупай',bestDeal,bestInterest,'RUB_RUB_buy')

        # продажа фиатных рублей за рубли
        bestDeal = getBestDeal('RUB','RUB',RUB_payTypes,'SELL')
        bestInterest = makeRateNicer(bestDeal['price']-1)
        showBestDeal(f'🔵⬇️\nФиат RUB/RUB: продавай',bestDeal,bestInterest,'RUB_RUB_sell',False)
    except Exception:
        sendNotification('Ошибка получения списка ордеров')

    sleep(waitTime) # ждем, чтобы начать новую проверку
